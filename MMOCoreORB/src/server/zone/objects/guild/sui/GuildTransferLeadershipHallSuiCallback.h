/*
 * GuildTransferLeadershipHallSuiCallback.h
 *
 *  Created on: May, 6 2015
 *      Author: Aso
 */

#ifndef GUILDTRANSFERLEADERSHIPHALLSUICALLBACK_H_
#define GUILDTRANSFERLEADERSHIPHALLSUICALLBACK_H_
//#include "server/zone/managers/guild/GuildManager.h"
#include "server/zone/objects/player/sui/SuiCallback.h"

class GuildTransferLeadershipHallSuiCallback : public SuiCallback {
public:
	GuildTransferLeadershipHallSuiCallback(ZoneServer* server)
		: SuiCallback(server) {
	}

	void run(CreatureObject* player, SuiBox* suiBox, bool cancelPressed, Vector<UnicodeString>* args) {

		if (!suiBox->isInputBox() || cancelPressed)
			return;

		if (args->size() < 1)
					return;

		String newOwnerName = args->get(0).toString();

		ManagedReference<GuildManager*> guildManager = server->getGuildManager();

		if (guildManager == NULL)
			return;

		guildManager->sendTransferLeaderHallAckTo(player, newOwnerName, suiBox->getUsingObject().get());
	}
};

#endif /* GUILDTRANSFERLEADERSHIPHALLSUICALLBACK_H_ */
