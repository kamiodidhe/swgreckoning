/*
 * CharacterBuildterTerminalTemplate.h
 *
 *  Created on: 06/05/2010
 *      Author: victor
 */

#ifndef CHARACTERBUILDTERTERMINALTEMPLATE_H_
#define CHARACTERBUILDTERTERMINALTEMPLATE_H_

#include "../SharedTangibleObjectTemplate.h"
#include "server/zone/objects/tangible/terminal/characterbuilder/CharacterBuilderMenuNode.h"

class CharacterBuilderTerminalTemplate : public SharedTangibleObjectTemplate {
	Reference<CharacterBuilderMenuNode*> rootNode;
	int builderperformanceBuff;
	int buildermedicalBuff;
	int builderperformanceDuration;
	int buildermedicalDuration;

public:
	CharacterBuilderTerminalTemplate() : rootNode(NULL), builderperformanceBuff(0),
		buildermedicalBuff(0), builderperformanceDuration(0), buildermedicalDuration(0) {
	}

	~CharacterBuilderTerminalTemplate() {
		if (rootNode != NULL) {
			//delete rootNode;
			rootNode = NULL;
		}
	}

	void readObject(LuaObject* templateData) {
		SharedTangibleObjectTemplate::readObject(templateData);

		builderperformanceBuff = templateData->getIntField("builderperformanceBuff");
		buildermedicalBuff = templateData->getIntField("buildermedicalBuff");
		builderperformanceDuration = templateData->getIntField("builderperformanceDuration");
		buildermedicalDuration = templateData->getIntField("buildermedicalDuration");

		LuaObject luaItemList = templateData->getObjectField("itemList");

		//Ensure that the luaItemList root level is of an even order.
		if (luaItemList.getTableSize() % 2 != 0) {
			System::out << "[CharacterBuilderTerminalTemplate] Dimension mismatch in itemList. Item count must be a multiple of 2." << endl;
			luaItemList.pop();
			return;
		}

		rootNode = new CharacterBuilderMenuNode("root");
		rootNode->readLuaObject(luaItemList, true);

		luaItemList.pop();
    }

    inline CharacterBuilderMenuNode* getItemList() const {
        return rootNode;
    }

    inline int getPerformanceBuff() const {
    	return builderperformanceBuff;
    }

    inline int getMedicalBuff() const {
    	return buildermedicalBuff;
    }

    inline int getPerformanceDuration() const {
    	return builderperformanceDuration;
    }

    inline int getMedicalDuration() const {
    	return buildermedicalDuration;
    }

};



#endif /* CHARACTERBUILDTERTERMINALTEMPLATE_H_ */
